<?php
interface Validate
{
    public function setName();
    public function searchName(int $userName, array $array): array;
}
