<?php

require_once 'Master.php';

class Run extends Master
{
    private string $name;
    private array $array = [['name' => 'visit', 'score' => 78], ['name' => 'rakesh', 'score' => 80], ['name' => 'rupesh', 'score' => 79]];
    private array $finalMessage;

    public function __construct()
    {
        $execute = new Master();
        // $this->number = $this->setNumber();
        $this->setName();
        $this->finalMessage = $execute->searchName($this->name, $this->array);
    }

    public function setName()
    {
        $userName = readline('Enter a name: ');
        $this->name = $userName;
    }

    public function getMessage()
    {
        var_dump($this->finalMessage);
    }
}

$run = new Run();
$run->getMessage();
