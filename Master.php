<?php

require_once 'Validate.php';

class Master implements Validate
{
    protected array $student;
    private bool $exists;

    public function setName()
    {
        $userName = readline('Enter a name: ');
        return $userName;
    }

    public function searchName($userName, $array): array
    {
        $this->exists = false;
        try {
            $this->student = array_filter(array_map(function ($singleStudent) use ($userName) {
                if ($singleStudent['name'] == $userName) {
                    $this->exists = true;
                    return $singleStudent;
                } else {
                }
            }, $array));

            if (!$this->exists) {
                throw new Exception('Name not found');
            }
        } catch (Exception $e) {
            return  ['Caught exception: ' .  $e->getMessage()];
        }

        return $this->student;
    }
}
